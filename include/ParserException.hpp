/**
 * 	Mire - The Scripting Language
 * 
 * 	File: ParserException.hpp
 * 	Author: Philipp Gildein
 * 	Last-Modified: 18.09.2007
 */

#ifndef __MIRE_PARSER_EXCEPTION_HPP__
#define __MIRE_PARSER_EXCEPTION_HPP__

#include <exception>

namespace mire
{
	class ParserException : public std::exception
	{
	 public:
	 	ParserException(const std::string &msg) : std::exception(), mMsg(msg) { }
	 	virtual ~ParserException(void) throw() { }
	 	
	 	virtual std::string what(void) { return mMsg; }
	 protected:
	 	std::string mMsg;
	 		
	};
}

#endif
