/**
 * 	Mire - The Scripting Language
 * 
 * 	File: main.cpp
 * 	Author: Philipp Gildein
 * 	Last-Modified: 18.09.2007
 * 
 * 	Mire is released under the terms of the GNU General Public License.
 * 	See COPYING for more informations.
 */

#include <iostream>
#include <fstream>

#include "Parser.hpp"
#include "ParserException.hpp"

#define Error(x) { std::cerr << "Error: " << x << "\n"; exit(-1); }

int main(int argc, char* argv[])
{
	// Check if a file got specified
	if (argc < 2)
		Error("No scriptfile supplied");	
	
	mire::Parser *p = 0;
	try
	{
		p = new mire::Parser();
		p->parseCmdLine(argc, argv);
		delete p;
		p = 0;
	}
	catch (mire::ParserException &e)
	{
		std::cerr << "Error: " << e.what() << "\n";
		if (p)
			delete p;
	}

	return 0;
}
