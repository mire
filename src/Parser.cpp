/**
 * 	Mire - The Scripting Language
 * 
 * 	File: Parser.cpp
 * 	Author: Philipp Gildein
 * 	Last-Modified: 18.09.2007
 *  
 * 	Mire is released under the terms of the GNU General Public License.
 * 	See COPYING for more informations.
 */

#include "Parser.hpp"
#include "ParserException.hpp"

#include <fstream>
#include <iostream>

namespace mire
{
	
	Parser::Parser()
	{
		//mSyntax = new Syntax();
	}
	
	Parser::~Parser()
	{
		//delete mSyntax;
	}
	
	void Parser::parseCmdLine(int argc, char** argv)
	{
		for (int i = 1; i < argc; ++i)
		{
			if (strcmp(argv[i], "--help") == 0)
				std::cout << "To be implemented\n";
			else if (strcmp(argv[i], "-w") == 0)
				std::cout << "To be implemented\n"; // Turn on all warnings
			else
				parseFile(argv[i]);
		}
	}
	
	void Parser::parseFile(const std::string &filename)
	{
		std::cerr << "[MIRE] Opening File "<<filename<<"...\n";
		std::ifstream file;
		std::string buffer;
		file.open(filename.c_str(), std::ios::in);
		if (!file.is_open())
			throw mire::ParserException("File couldn't be found");
		while (!file.eof())
		{
			char buf;
			file.get(buf);
			buffer += buf;
		}
		
		if (mire::parseFile(buffer))
			std::cerr << "File successfully parsed\n";
		else 
			std::cerr << "File got errors\n";		
		
		//boost::spirit::parse_info<> info = boost::spirit::parse(buffer.begin(), buffer.end(), *mSyntax, boost::spirit::space_p );
	}
	
}
