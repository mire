/**
 * 	Mire - The Scripting Language
 * 
 * 	File: Syntax.hpp
 * 	Author: Philipp Gildein
 * 	Last-Modified: 18.09.2007
 * 
 *  	Mire is released under the terms of the GNU General Public License.
 * 	See COPYING for more informations.
 */

#ifndef __MIRE_SYNTAX_HPP__
#define __MIRE_SYNTAX_HPP__

#include <string>

namespace mire
{

	bool parseFile(const std::string &content);
	
}

#endif
