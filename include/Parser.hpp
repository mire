/**
 * 	Mire - The Scripting Language
 * 
 * 	File: Parser.hpp
 * 	Author: Philipp Gildein
 * 	Last-Modified: 18.09.2007
 * 
 * 	Mire is released under the terms of the GNU General Public License.
 * 	See COPYING for more informations.
 */

#ifndef __MIRE_PARSER_HPP__
#define __MIRE_PARSER_HPP__

#include "Syntax.hpp"

#include <string>

namespace mire
{
	
	class Parser
	{
	 public:
	 	Parser(void);
	 	~Parser(void);
	 	
	 	void parseCmdLine(int argc, char** argv);
	 	void parseFile(const std::string &filename);	 
	 protected:
	 	//Syntax	*mSyntax;
	};
	
}

#endif
