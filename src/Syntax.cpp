/**
 * 	Mire - The Scripting Language
 * 
 * 	File: Syntax.cpp
 * 	Authors: Philipp Gildein
 * 	Last-Modified: 19.09.2007
 *
 * 	Mire is released under the terms of the GNU General Public License.
 * 	See COPYING for more informations.
 */

#include "Syntax.hpp"
#include "Command.hpp"

#define BOOST_SPIRIT_DEBUG
#include <boost/spirit.hpp>

using namespace boost::spirit;

namespace mire
{
	
	class SkipSyntax : public boost::spirit::grammar<SkipSyntax>
	{
	 public:
	 	template<typename ScanT>
	 	struct definition
	 	{
	 		rule<ScanT> skip;
	 		definition(SkipSyntax const &self)
	 		{
	 			skip = space_p | confix_p("##",*anychar_p, "##") | comment_p('#');
				BOOST_SPIRIT_DEBUG_RULE(skip);
	 		}
	 		
	 		const rule<ScanT>& start()
	 		{
	 			return skip;
	 		}	
	 	};
	};
	
	class Syntax : public boost::spirit::grammar<Syntax>
	{
	 public:
	 	template<typename ScanT>
	 	struct definition
	 	{
	 		rule<ScanT> lines;
	 		definition(Syntax const &self)	
	 		{
	 		/* Some basic rules */
				rule<ScanT> ident = alpha_p | ch_p('_') >> *(alnum_p | ch_p('_'));
				rule<ScanT> blanks_p = * blank_p;
				rule<ScanT> line_empty = blanks_p >> eol_p;
				rule<ScanT> ending = ch_p(';') | eol_p;
				
			/* A one-line comment */
				//rule<ScanT> line_comment = blanks_p >> ch_p('#') >> *print_p >> eol_p;
		
			/* A multi-line comment */
				//rule<ScanT> mline_comment = confix_p("##",*anychar_p, "##");
				
				rule<ScanT> line = line_empty | ending;
				lines = *line;
				
				BOOST_SPIRIT_DEBUG_RULE(ident);
				BOOST_SPIRIT_DEBUG_RULE(ending);
				//BOOST_SPIRIT_DEBUG_RULE(line_comment);
				//BOOST_SPIRIT_DEBUG_RULE(mline_comment);
				BOOST_SPIRIT_DEBUG_RULE(line);
	 		}
	 		
	 		const boost::spirit::rule<ScanT>& start()
	 		{
	 			return lines;
	 		}
	 	};
		
	};
	
	bool parseFile(const std::string &content)
	{
		Syntax syntax;
		SkipSyntax skip;
		std::cerr << "[MIRE] Parsing...\n";
		BOOST_SPIRIT_DEBUG_GRAMMAR(syntax);
		BOOST_SPIRIT_DEBUG_GRAMMAR(skip);
		parse_info<> parseinf = parse(content.c_str(), syntax, skip);
		std::cerr << "[MIRE] Finished parsing...\n";
		return parseinf.full;
	}
	
}
