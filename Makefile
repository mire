all:
	g++ -c src/main.cpp -ggdb3 -Iinclude
	g++ -c src/Parser.cpp -ggdb3 -Iinclude
	g++ -c src/Syntax.cpp -ggdb3 -Iinclude -I/usr/include
	g++ -c src/Command.cpp -ggdb3 -Iinclude
	g++ -o mire -ggdb3 main.o Parser.o Syntax.o Command.o
