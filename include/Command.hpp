/**
 * 	Mire - The Scripting Language
 * 
 * 	File: Syntax.cpp
 * 	Authors: Philipp Gildein
 * 	Last-Modified: 19.09.2007
 *
 * 	Mire is released under the terms of the GNU General Public License.
 * 	See COPYING for more informations.
 */

#ifndef __MIRE_COMMAND_HPP__
#define __MIRE_COMMAND_HPP__

#include <string>
#include <vector>

namespace mire
{
	
	class Command
	{
	 public:
	 	Command(void) { }
	 	~Command(void) { }
	 	
	 	void addCommand(const std::string &ident);
	 	void addArgument(const std::string &arg);
	 	void executeCommand(void);
	 protected:
	 	std::string mCommand;
	 	std::vector<std::string> mArguments;
	};
	
}

#endif
