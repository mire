/**
 * 	Mire - The Scripting Language
 * 
 * 	File: Syntax.cpp
 * 	Authors: Philipp Gildein
 * 	Last-Modified: 19.09.2007
 *
 * 	Mire is released under the terms of the GNU General Public License.
 * 	See COPYING for more informations.
 */

#include "Command.hpp"
#include "ParserException.hpp"

#include <iostream>

namespace mire
{
	
	void Command::addCommand(const std::string &ident)
	{
		if (!mCommand.empty() || !mArguments.empty())
			throw ParserException("New command before old one got executed");
		mCommand = ident;	
	}
	
	void Command::addArgument(const std::string &arg)
	{
		if (mCommand.empty())
			throw ParserException("Tried to add argument without a command");
		mArguments.push_back(arg);
	}
	
	void Command::executeCommand()
	{
		// TODO: This needs to be replaced with something MUCH more dynamic :)
		if (mCommand == "echo")
		{
			for (int i = 0; i < mArguments.size(); ++i)
				std::cout << mArguments[i];
		}
		else
		{
			throw ParserException("Wrong/Unknown command '"+mCommand+"'");
		}
		
		mCommand = "";		
		while(!mArguments.empty())
			mArguments.pop_back();
	}
}
